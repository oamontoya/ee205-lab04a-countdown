//////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Osiel Montoya <montoyao@hawaii.edu>
// @date   02_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define SECONDSINYEAR 31536000
#define SECONDSINDAY 86400
#define SECONDSINHOUR 3600
#define SECONDSINMINUTE 60

time_t SetTargetDate();
void printTimeDifference(time_t);
double subtractYears(double);
double subtractDays(double);
double subtractHours(double);
double subtractMinutes(double);

int main(int argc, char* argv[]) {
   time_t birthday = SetTargetDate();
   
   printf("Reference time: %s\n", ctime(&birthday));
   
   while(1){
   printTimeDifference(birthday);
   sleep(1); 
   }
   
   return 0;
}

time_t SetTargetDate(){
   struct tm targetDate;
   targetDate.tm_sec = 0;
   targetDate.tm_min = 0;
   targetDate.tm_hour = 9;
   targetDate.tm_mday = 23;
   targetDate.tm_mon = 7;
   targetDate.tm_year = 93;
   targetDate.tm_isdst = 0; 

   return mktime(&targetDate);
}

void printTimeDifference(time_t timeToBeCompared){
   time_t now = time(NULL);
   double timeDifferenceInSeconds = difftime(now, timeToBeCompared);
   
   if(timeDifferenceInSeconds <= 0){
      timeDifferenceInSeconds *= -1;
   }

   timeDifferenceInSeconds = subtractYears(timeDifferenceInSeconds);
   timeDifferenceInSeconds = subtractDays(timeDifferenceInSeconds);
   timeDifferenceInSeconds = subtractHours(timeDifferenceInSeconds);
   timeDifferenceInSeconds = subtractMinutes(timeDifferenceInSeconds);
   printf("Seconds: %.0f\n", timeDifferenceInSeconds);

}

double subtractYears(double yearsDifference){
   double remainingTime;
   int numberOfYears = yearsDifference / SECONDSINYEAR ;
   //NOTE: I am dividing a double by an int and concatinating the result by saving it to an int
   printf("Years: %d\t", numberOfYears);
   return yearsDifference - (numberOfYears * SECONDSINYEAR );
}

double subtractDays(double daysDifference){
   double remainingTime;
   int numberOfDays = daysDifference / SECONDSINDAY;
   //NOTE: I am dividing a double by an int and concatinating the result by saving it to an int
   printf("Days: %d\t", numberOfDays);
   return daysDifference - (numberOfDays * SECONDSINDAY);
}

double subtractHours(double hoursDifference){
   double remainingTime;
   int numberOfHours = hoursDifference / SECONDSINHOUR;
   //NOTE: I am dividing a double by an int and concatinating the result by saving it to an int
   printf("Hours: %d\t", numberOfHours);
   return hoursDifference - (numberOfHours * SECONDSINHOUR);
}

double subtractMinutes(double minutesDifference){
   double remainingTime;
   int numberOfMinutes = minutesDifference / SECONDSINMINUTE;
   //NOTE: I am dividing a double by an int and concatinating the result by saving it to an int
   printf("Minutes: %d\t", numberOfMinutes);
   return minutesDifference - (numberOfMinutes * SECONDSINMINUTE);
}

